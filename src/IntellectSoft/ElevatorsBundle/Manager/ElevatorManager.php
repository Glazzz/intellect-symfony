<?php

namespace IntellectSoft\ElevatorsBundle\Manager;

use IntellectSoft\ElevatorsBundle\Entity\Elevator;

class ElevatorManager extends BaseManager
{
    /**
     * @param int $currentFloor
     * @param int $destination
     * @return bool|Elevator
     */
    public function getForFloor($currentFloor, $destination)
    {
        /** @var \IntellectSoft\ElevatorsBundle\Repository\ElevatorRepository $repository */
        $repository = $this->getRepository('IntellectSoftElevatorsBundle:Elevator');

        $qb = $repository->getActivate();

        $qb
            ->where('e.maxFloor >= :destination')
            ->andWhere('e.maxFloor >= :currentFloor')
            ->setParameters([
                ':destination' => $destination,
                ':currentFloor' => $currentFloor,
            ])
        ;

        $data = $qb->getQuery()->getResult();

        if (!$data) {
            return false;
        }

        usort($data, function($a, $b) use($currentFloor) {
            return abs($currentFloor - $a->getCurrentFloor()) <=> abs($currentFloor - $b->getCurrentFloor());
        });

        return $data[0];
    }
}