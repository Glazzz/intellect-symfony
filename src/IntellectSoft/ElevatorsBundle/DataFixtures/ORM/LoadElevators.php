<?php

namespace IntellectSoft\ElevatorsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use IntellectSoft\ElevatorsBundle\Entity\Elevator;

class LoadElevators implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            [
                'current_floor' => 1,
                'max_floor' => 25,
                'status' => Elevator::STATUS_ACTIVE,
            ],
            [
                'current_floor' => 1,
                'max_floor' => 25,
                'status' => Elevator::STATUS_ACTIVE,
            ],
            [
                'current_floor' => 1,
                'max_floor' => 15,
                'status' => Elevator::STATUS_ACTIVE,
            ],
            [
                'current_floor' => 1,
                'max_floor' => 15,
                'status' => Elevator::STATUS_ACTIVE,
            ],
        ];

        foreach ($data as $item) {
            $elevator = new Elevator();
            $elevator
                ->setCurrentFloor($item['current_floor'])
                ->setMaxFloor($item['max_floor'])
                ->setStatus($item['status'])
            ;
            $manager->persist($elevator);
        }
        
        $manager->flush();
    }
}