<?php

namespace IntellectSoft\ElevatorsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use \DateTime;

/**
 * Elevator
 *
 * @ORM\Table(name="elevators")
 * @ORM\Entity(repositoryClass="IntellectSoft\ElevatorsBundle\Repository\ElevatorRepository")
 */
class Elevator
{
    const TOTAL_FLOORS = 25;
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="current_floor", type="smallint", options={"unsigned"=true})
     * 
     * @Assert\NotBlank(message="Current floor should not be blank.")
     * @Assert\Type(type="smallint", message="Current floor should contain only numbers")
     * @Assert\Range(min=1, minMessage="Current floor should must be more than 0")
     */
    private $currentFloor = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="max_floor", type="smallint", options={"unsigned"=true})
     *
     * @Assert\NotBlank(message="Max floor should not be blank.")
     * @Assert\Type(type="smallint", message="Max floor should contain only numbers")
     * @Assert\Range(min=1, minMessage="Max floor should must be more than 0")
     */
    private $maxFloor = self::TOTAL_FLOORS;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     *
     * @Assert\NotBlank(message="Status should not be blank.")
     * @Assert\Type(type="smallint", message="Status should contain only numbers")
     * @Assert\Range(min=0, minMessage="Status should must be more than 0")
     */
    private $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * Elevator constructor.
     */
    public function __construct()
    {
        $this->createdAt = new DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currentFloor
     *
     * @param integer $currentFloor
     *
     * @return Elevator
     */
    public function setCurrentFloor($currentFloor)
    {
        $this->currentFloor = $currentFloor;

        return $this;
    }

    /**
     * Get currentFloor
     *
     * @return int
     */
    public function getCurrentFloor()
    {
        return $this->currentFloor;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Elevator
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return integer
     */
    public function getMaxFloor()
    {
        return $this->maxFloor;
    }

    /**
     * @param integer $maxFloor
     * @return $this
     */
    public function setMaxFloor($maxFloor)
    {
        $this->maxFloor = $maxFloor;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get all statuses
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => 'status.active',
            self::STATUS_DISABLED => 'status.disabled',
        ];
    }
}

