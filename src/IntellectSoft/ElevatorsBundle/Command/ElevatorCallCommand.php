<?php

namespace IntellectSoft\ElevatorsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use IntellectSoft\ElevatorsBundle\Entity\Elevator;

class ElevatorCallCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('elevator:call')
            ->setDescription('Call an elevator')
            ->addOption('currentFloor', null, InputOption::VALUE_OPTIONAL, 'Current floor', 0)
            ->addOption('destination', null, InputOption::VALUE_OPTIONAL, 'Destination', 0)
            ->addArgument('floor', InputArgument::OPTIONAL, 'Set a floor')
            ->setHelp('This command allows you to call an elevator')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \Symfony\Component\Console\Helper\QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $currentFloor = (int) $input->getOption('currentFloor');
        $destination = (int) $input->getOption('destination');

        // Enter current floor value if we didn't use option
        $question = new Question('Please enter the current floor number: ', 0);

        while ($currentFloor < 1 || $currentFloor > Elevator::TOTAL_FLOORS) {
            $currentFloor = (int) $helper->ask($input, $output, $question);
        }

        // Enter destination value if we didn't use option
        $question = new Question('Please enter a destination floor: ', 0);

        while($currentFloor === $destination || $destination < 1 || $destination > Elevator::TOTAL_FLOORS) {
            $destination = (int) $helper->ask($input, $output, $question);
        }

        $service = $this->getContainer()->get('intellect_soft_elevators.manager.elevator');
        $floor = $service->getForFloor($currentFloor, $destination);

        if (!$floor) {
            $output->writeln('Sorry, we not have elevators for you.');
            return ;
        }

        // Update current floor for the elevator.
        $floor->setCurrentFloor($destination);
        $service->saveEntity($floor);

        $output->writeln('<info>You have successfully arrived to destination floor.</info>');
        $output->writeln(sprintf('You used elevator: %d.', $floor->getId()));
    }

}
