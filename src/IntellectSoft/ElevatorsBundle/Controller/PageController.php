<?php

namespace IntellectSoft\ElevatorsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('IntellectSoftElevatorsBundle:Pages:index.html.twig');
    }
}
